/**
\page lua_api_input Inputs

\tableofcontents

You can get information about the low-level keyboard and joypad inputs
through \c sol.input and input handlers.

\c sol.input functions are used to check the current state of input while the
handlers are notified when an input event to change the state of input occurs.
Using an input handler is prefered, but you can use \c sol.input for the cases
where more information than a single event is needed.

Also note that during the game, there exists the higher-level notion of
\ref lua_api_game_overview_commands "game commands" to ease your life which
have their own higher-level events.
Commands are only generated from unhandled input, so handling a piece of input
will prevent the command from being activated.

\section lua_api_input_functions Functions of sol.input

\subsection lua_api_input_is_joypad_enabled sol.input.is_joypad_enabled()

Returns whether joypad support is enabled.

This may be true even without any joypad plugged.
- Return value (boolean): \c true if joypad support is enabled.

\subsection lua_api_input_set_joypad_enabled sol.input.set_joypad_enabled([joypad_enabled])

Enables or disables legacy joypad support. For new joypad support see \ref lua_api_joypad.

Joypad support may be enabled even without any joypad plugged.
- \c joypad_enabled \c true to enable joypad support, false to disable it.
  No value means \c true.

\subsection lua_api_input_is_key_pressed sol.input.is_key_pressed(key)

Returns whether a keyboard key is currently down.
- \c key (string): The name of a keyboard key.
- Return value (boolean): \c true if this keyboard key is currently down.

\subsection lua_api_input_get_key_modifiers sol.input.get_key_modifiers()

Returns the keyboard key modifiers currently active.
- Return value (table): A table whose keys indicate what modifiers are currently down.
  Possible table keys are \c "shift", \c "control", \c "alt" and \c "caps lock".
  Table values are \c true.

\subsection lua_api_input_is_joypad_button_pressed sol.input.is_joypad_button_pressed(button)

Returns whether a joypad button is currently down.
- \c button (number): Index of a button of the joypad.
- Return value (boolean): \c true if this joypad button is currently down.

\subsection lua_api_input_get_joypad_axis_state sol.input.get_joypad_axis_state(axis)

Returns the current state of an axis of the joypad.
- \c axis (number): Index of a joypad axis. The first one is \c 0.
- Return value (number): The state of that axis.
  \c -1 means left or up, \c 0 means centered and \c 1 means right or down.

\subsection lua_api_input_get_joypad_hat_direction sol.input.get_joypad_hat_direction(hat)

Returns the current direction of a hat of the joypad.
- \c hat (number): Index of a joypad hat. The first one is \c 0.
- Return value (number): The direction of that hat.
  \c -1 means that the hat is centered.
  \c 0 to \c 7 indicates that the hat is in one of the eight main directions.

\subsection lua_api_input_get_mouse_position sol.input.get_mouse_position()

Returns the current position of the mouse cursor relative to the quest size.

If the mouse is outside the window, mouse coordinates are captured
only if a mouse button is pressed.
In this case, the returned values can be out of bounds of the quest size
and can be negative.
This allows you to keep track of the mouse movement when dragging something.
Otherwise, when no mouse button is pressed,
the returned coordinates are the last position of the mouse in the window.
- Return value 1 (integer): The \c x position of the mouse in
  \ref lua_api_video_get_quest_size "quest size" coordinates.
- Return value 2 (integer): The \c y position of the mouse in
  \ref lua_api_video_get_quest_size "quest size" coordinates.

\subsection lua_api_input_is_mouse_button_pressed sol.input.is_mouse_button_pressed(button)

Returns whether a mouse button is currently down.
- \c button (string): The name of a mouse button.
  Possible values are \c "left", \c "middle", \c "right", \c "x1" and \c "x2".
- Return value (boolean): \c true if mouse button is down.

\subsection lua_api_input_get_finger_position sol.input.get_finger_position(finger)

Returns the current position of a finger if it exists.
- \c finger (integer): The finger id to check.
- Return value 1 (integer): The \c x position of the finger in
  \ref lua_api_video_get_quest_size "quest size" coordinates.
  Return \c nil if the finger does not exist or is not pressed.
- Return value 2 (integer): The \c y position of the finger in
  \ref lua_api_video_get_quest_size "quest size" coordinates.

\subsection lua_api_input_get_finger_pressure sol.input.get_finger_pressure(finger)

Returns the current pressure of a finger if it exists.
- \c finger (integer): The finger id to check.
- Return value 1 (number): The \c pressure of the finger
  between <tt>0.0</tt> and <tt>1.0</tt>.
  Return \c nil if there is no such finger.

\subsection lua_api_input_is_finger_pressed sol.input.is_finger_pressed(finger)

Returns whether a finger is currently pressed.
- \c finger (integer): The finger id to check.
- Return value (boolean): \c true if the finger is down.

\subsection lua_api_input_simulate_key_pressed sol.input.simulate_key_pressed(key)

Simulates pressing a keyboard key.
- \c key (string): The keyboard key to simulate.

\subsection lua_api_input_simulate_key_released sol.input.simulate_key_released(key)

Simulates releasing a keyboard key.
- \c key (string): The keyboard key to simulate.

\subsection lua_api_input_get_joypad_count sol.input.get_joypad_count()

- Return value (number): The amount of connected joypads.

\subsection lua_api_input_get_joypads sol.input.get_joypads()

- Return value (array of \ref lua_api_joypad): Array of connected joypad objects.

\section lua_api_input_events Events of sol.input

\subsection lua_api_input_on_joypad_connected sol.input.on_joypad_connected(joypad)

- \c joypad (\ref lua_api_joypad): a hot plugged joypad.

\section lua_api_input_handler_events Events of Input Handlers

There are several lua types that act as input handlers. These are
\ref lua_api_main "sol.main",
the running \ref lua_api_game "game",
the active \ref lua_api_map "map",
the hero's current \ref lua_api_state "custom state" and
any \ref lua_api_menu "menus" in the context of another event handler.
When input occurs the first four (main, game, map and state) are checked in
that order with menus in between. Menus are checked after their context unless
their context is another menu then they are checked before. There is no strict
ordering between menus in the same context.

When an input handler is checked if it has the approprate event
defined on it the event is called. If the handler returns true the input is
fully handled and propogation stops. If there is no handler or the event
returns false or nothing then the next handler is checked.

\remark It is only after all of these handlers are checked and none of them
handle the low-level input that the input is convered into a
\ref lua_api_game_overview_commands "game command".

\subsection lua_api_input_on_key_pressed handler:on_key_pressed(key, modifiers)

Called when the user presses a keyboard key while the handler is active.
- \c key (string): Name of the raw key that was pressed.
- \c modifiers (table): A table whose keys indicate what modifiers were
  down during the event. Possible table keys are \c "shift", \c "control" and
  \c "alt". Table values are \c true.
- Return value (boolean): Indicates whether the event was handled. If you
  return \c true, the event won't be propagated to other objects.
  If you return \c false or nothing, the event will continue its propagation.

\remark This event indicates the raw keyboard key pressed. If you want the
corresponding character instead (if any), see
\ref lua_api_input_on_character_pressed "handler:on_character_pressed()".
If you want the corresponding higher-level game command (if any), check if
the handler also supports an \c on_command_pressed event.

\subsection lua_api_input_on_key_released handler:on_key_released(key)

Called when the user releases a keyboard key while the handler is active.
- \c key (string): Name of the raw key that was released.
- Return value (boolean): Indicates whether the event was handled. If you
  return \c true, the event won't be propagated to other objects.
  If you return \c false or nothing, the event will continue its propagation.

\remark This event indicates the raw keyboard key released.
If you want the corresponding higher-level game command (if any), check if
the handler also supports an \c on_command_released event.

\subsection lua_api_input_on_character_pressed handler:on_character_pressed(character)

Called when the user enters text while the handler is active.
- \c character (string): A utf-8 string representing the character that was
  pressed.
- Return value (boolean): Indicates whether the event was handled. If you
  return \c true, the event won't be propagated to other objects.
  If you return \c false or nothing, the event will continue its propagation.

\remark When a character key is pressed, two events are called:
\ref lua_api_input_on_key_pressed "handler:on_key_pressed()"
(indicating the raw key)
and \ref lua_api_input_on_character_pressed "handler:on_character_pressed()"
(indicating the utf-8 character).
If your script needs to input text from the user,
\ref lua_api_input_on_character_pressed "handler:on_character_pressed()"
is what you want because it
considers the keyboard's layout and gives you international utf-8
strings.

\subsection lua_api_input_on_joypad_button_pressed handler:on_joypad_button_pressed(button, joypad)

Called when the user presses a joypad button while the handler is active.
- \c button (string): name of the button that was pressed.
- \c joypad (\ref lua_api_joypad): Joypad that triggered the event
- Return value (boolean): Indicates whether the event was handled. If you
  return \c true, the event won't be propagated to other objects.
  If you return \c false or nothing, the event will continue its propagation.

\subsection lua_api_input_on_joypad_button_released handler:on_joypad_button_released(button)

Called when the user releases a joypad button while the handler is active.
- \c button (string): Name of the button that was released.
- \c joypad (\ref lua_api_joypad): Joypad that triggered the event
- Return value (boolean): Indicates whether the event was handled. If you
  return \c true, the event won't be propagated to other objects.
  If you return \c false or nothing, the event will continue its propagation.

\subsection lua_api_input_on_joypad_axis_moved handler:on_joypad_axis_moved(axis, state, joypad)

Called when the user moves a joypad axis while the handler is active.
- \c axis (string): Name of the axis that was moved. Usually, \c 0 is an
  horizontal axis and \c 1 is a vertical axis.
- \c state (number): The new state of the axis that was moved. \c -1 means
  left or up, \c 0 means centered and \c 1 means right or down.
- \c joypad (\ref lua_api_joypad): Joypad that triggered the event
- Return value (boolean): Indicates whether the event was handled. If you
  return \c true, the event won't be propagated to other objects.
  If you return \c false or nothing, the event will continue its propagation.

\subsection lua_api_input_on_joypad_hat_moved handler:on_joypad_hat_moved(hat, direction8, joypad)

\remark DEPRECATED, this event is not triggered anymore, hat directions are reported as buttons.

Called when the user moves a joypad hat while the handler is active.
- \c hat (number): Index of the hat that was moved.
- \c direction8 (number): The new direction of the hat. \c -1 means that the
  hat is centered. \c 0 to \c 7 indicates that the hat is in one of the eight
  main directions.
- \c joypad (\ref lua_api_joypad): Joypad that triggered the event
- Return value (boolean): Indicates whether the event was handled. If you
  return \c true, the event won't be propagated to other objects.
  If you return \c false or nothing, the event will continue its propagation.

\subsection lua_api_input_on_mouse_pressed handler:on_mouse_pressed(button, x, y)

Called when the user presses a mouse button while the handler is active.
- \c button (string): Name of the mouse button that was pressed.
  Possible values are \c "left", \c "middle", \c "right", \c "x1" and \c "x2".
- \c x (integer): The x position of the mouse in
  \ref lua_api_video_get_quest_size "quest size" coordinates.
- \c y (integer): The y position of the mouse in
  \ref lua_api_video_get_quest_size "quest size" coordinates.
- Return value (boolean): Indicates whether the event was handled. If you
  return \c true, the event won't be propagated to other objects.
  If you return \c false or nothing, the event will continue its propagation.

\subsection lua_api_input_on_mouse_released handler:on_mouse_released(button, x, y)

Called when the user releases a mouse button while the handler is active.
- \c button (string): Name of the mouse button that was released.
  Possible values are \c "left", \c "middle", \c "right", \c "x1" and \c "x2".
- \c x (integer): The x position of the mouse in
  \ref lua_api_video_get_quest_size "quest size" coordinates.
- \c y (integer): The y position of the mouse in
  \ref lua_api_video_get_quest_size "quest size" coordinates.
- Return value (boolean): Indicates whether the event was handled. If you
  return \c true, the event won't be propagated to other objects.
  If you return \c false or nothing, the event will continue its propagation.

\subsection lua_api_input_on_finger_pressed handler:on_finger_pressed(finger, x, y, pressure)

Called when the user presses a finger while the handler is active.
- \c finger (integer): ID of the finger that was pressed.
- \c x (integer): The x position of the finger in
  \ref lua_api_video_get_quest_size "quest size" coordinates.
- \c y (integer): The y position of the finger in
  \ref lua_api_video_get_quest_size "quest size" coordinates.
- \c pressure (number): The pressure of the finger, normalized between 0 and 1.
- Return value (boolean): Indicates whether the event was handled. If you
  return \c true, the event won't be propagated to other objects.
  If you return \c false or nothing, the event will continue its propagation.

\subsection lua_api_input_on_finger_released handler:on_finger_released(finger, x, y, pressure)

Called when the user releases a finger while the handler is active.
- \c finger (integer): ID of the finger that was pressed.
- \c x (integer): The x position of the finger in
  \ref lua_api_video_get_quest_size "quest size" coordinates.
- \c y (integer): The y position of the finger in
  \ref lua_api_video_get_quest_size "quest size" coordinates.
- \c pressure (number): The pressure of the finger, normalized between 0 and 1.
- Return value (boolean): Indicates whether the event was handled. If you
  return \c true, the event won't be propagated to other objects.
  If you return \c false or nothing, the event will continue its propagation.

\subsection lua_api_input_on_finger_moved handler:on_finger_moved(finger, x, y, dx, dy, pressure)

Called when the user moves a finger while the handler is active.
- \c finger (integer): ID of the finger that was pressed.
- \c x (integer): The x position of the finger in
  \ref lua_api_video_get_quest_size "quest size" coordinates.
- \c y (integer): The y position of the finger in
  \ref lua_api_video_get_quest_size "quest size" coordinates.
- \c dx (integer): The horizontal distance moved by finger in
  \ref lua_api_video_get_quest_size "quest size" coordinates.
- \c dy (integer): The vertical distance moved by finger in
  \ref lua_api_video_get_quest_size "quest size" coordinates.
- \c pressure (number): The pressure of the finger, normalized between 0 and 1.
- Return value (boolean): Indicates whether the event was handled. If you
  return \c true, the event won't be propagated to other objects.
  If you return \c false or nothing, the event will continue its propagation.

*/
