/**
\page lua_api_sound Sound

\tableofcontents

You can create, play, or pause sound effects through \c sol.sound.

An object of type \c sound represents a sound effect with its current playing
state.
Multiple sounds effects can be played, paused, resumed or
stopped independently, even from the same sound file.

\section lua_api_sound_functions Functions of sol.sound

\subsection lua_api_sound_create sol.sound.create(sound_id)

Creates a new sound effect but does not play it.

Generates a Lua error if the sound file does not exist.

Unlike music files, sound files are entirely read before being played.
- \c sound_id (string): Name of the sound file to read, relative to
  the \c sounds directory and without extension.
  Currently, only OGG Vorbis (<tt>.ogg</tt>) sound files are supported.

\section lua_api_sound_methods Methods of sol.sound

\subsection lua_api_sound_play sound:play()

Starts playing the sound effect.

\subsection lua_api_sound_stop sound:stop()

Stops playing the sound effect.

\subsection lua_api_sound_is_paused sound:is_paused()

Returns whether the sound effect is currently paused.
- Return value (boolean): \c true if the sound is paused, \c false otherwise.

\subsection lua_api_sound_set_paused sound:set_paused([paused])

Pauses or resumes the sound effect.
- \c paused (boolean, optional): \c true to pause the sound, \c false to
  unpause it. No value means \c true.

\subsection lua_api_sound_get_volume sound:get_volume()

Returns the volume of this sound effect.

This is relative to the global
\ref lua_api_audio_get_sound_volume "sound volume".
- Return value (number): The current volume of this sound effect,
  as an integer between \c 0 (mute) and \c 100 (full volume).

\subsection lua_api_sound_set_volume sound:set_volume(volume)

Sets the volume of this sound effect.

This is relative to the global
\ref lua_api_audio_set_sound_volume "sound volume".
- \c volume (number): The new volume of this sound effect,
  as an integer between \c 0 (mute) and \c 100 (full volume).

\subsection lua_api_sound_get_pan sound:get_pan()

Returns the pan value of this sound effect.
- Return value (number): The current pan of this sound effect,
  as a number between \c -1.0 (left) and \c 1.0 (right).

\subsection lua_api_sound_set_pan sound:set_pan(pan)

Sets the pan of this sound effect.

This has no effect on stereo sounds.
- \c pan (number): The new pan of this sound effect,
  as a number between \c -1.0 (left) and \c 1.0 (right).

\subsection lua_api_sound_get_pitch sound:get_pitch()

Returns the pitch value of this sound effect.
- Return value (number): The current pitch of this sound effect,
  as a number between \c 0.5 and \c 2.0.

\subsection lua_api_sound_set_pitch sound:set_pitch(pitch)

Sets the pitch of this sound effect.

The value acts as a sample rate multiplier.
- \c pitch (number): The new pitch of this sound effect,
  as a number between \c 0.5 and \c 2.0.

*/