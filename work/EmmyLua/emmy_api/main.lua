---@class main
---`sol.main` contains general features and utility functions that are global to the execution of the program, no matter if a game or some menus are running.
---
local m = {}

_G.main = m

return m